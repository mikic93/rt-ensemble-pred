# RT-Eensemble-Pred
A tool for retention time prediction of LC-MS


The interface and its depend-upon packages were all wrapped in the RT-Ensemble Pred version 1.0 Package. User can click the ./dist/main.exe to excute the calculation.


Four molecular descriptors would be needed, including Chemopy, Pybel, RDKit and PaDEL. Molecular descriptors of Chemopy and Pybel can be calculated by  ChemDes (http://www.scbdd.com/chemdes/). Molecular descriptors of RDKit and PaDEL can be calculated by Python (rdkit and padelpy packages) or by  ChemDes (http://www.scbdd.com/chemdes/).
Variables of liquid chromatographic methods (CMs) should be needed. 

variables of CMs and molecular descriptors were all needed to add a specific prefix. Each variable of CMs should be prefixed 'CM_'. Each molecular descriptors of Chemopy should be prefixed 'Chemopy_'. Each molecular descriptors of Pybel should be prefixed 'Pybel_'. Each molecular descriptors of RDKit should be prefixed 'Rdkit_'. Each molecular descriptors of PaDEL should be prefixed 'Padel_'. The template can be checked in the test data.csv file.

The test data uploaed should be a .csv file.


Note:

RT-Ensemble Pred is still under testing. So far, it can be used in Windows 10 and more systems are still being tested.

For additional support/questions, contact Biying Chen (chenby93@126.com).
